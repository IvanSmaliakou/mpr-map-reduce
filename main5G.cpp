#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <map>
#include <chrono>

using namespace std;

bool cmp_fn(pair<string, int> el1, pair<string, int> el2)
{
    return el1.second > el2.second;
}

void word_count(string filename)
{
    std::ifstream fs(filename);
    if (!fs.is_open()){
        cout << "error reading file\n";
        return;
    }
    map<std::string, int> count_map;
    string temp_word;
    while (fs >> temp_word)
    {
        if (count_map.find(temp_word) == count_map.end())
        {
            count_map[temp_word] = 1;
        } else {
            count_map[temp_word]++;
        }
    }
    fs.close();
    ofstream outfile("res.txt");
    vector<pair<string, int> > res_vec;
    for (auto it = count_map.begin(); it != count_map.end(); it++) {
        res_vec.push_back(pair<string, int>(it->first, it->second));
    }
    for (const auto& it: res_vec) {
        outfile << it.first << ": " << it.second << endl; 
    }
    outfile.close();
    return;
}

int main(int argc, char* argv[])
{
    auto start = std::chrono::high_resolution_clock::now();
    word_count("gutenberg-5G.txt");
    auto end = std::chrono::high_resolution_clock::now();
    std::ofstream outfile ("gutenberg-5G.res.txt");
    outfile << "Execution timeof gutenberg-5G.txt file: " << (end-start).count() / 1e9 << " seconds." << std::endl;
}
